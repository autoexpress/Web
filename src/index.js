import React from 'react';
import ReactDOM from 'react-dom';
import ApplicationRoutes from './AppRoutes';


import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

ReactDOM.render(
    
   <ApplicationRoutes/>,
   
    document.getElementById('root'));
